/*
*   Async Await
*/

// let getNombre = async () =>{
//     undefined.nombre
//     return 'Fernando';
// };

let getNombre = () => {
    return new Promise((resolve, reject) => {
        setTimeout( () => {
            resolve('Fernando');
        }, 3000);
    });
}

let saludo = async () => {

    let nombre = await getNombre();

    return `Hola ${ nombre }`;
}

// console.log(getNombre());

// getNombre().then(nombre => console.log(nombre))
//             .catch(e => console.log('Error en ASYNC ', e));

saludo().then(msj => console.log(msj));